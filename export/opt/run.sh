#!/bin/bash
if [ -f /opt/corvus/corvus.conf ]; then
echo Using config file /opt/corvus/corvus.conf
else
echo Error: No config file /opt/corvus/corvus.conf
exit 1
fi

exec /opt/corvus/corvus /opt/corvus/corvus.conf
