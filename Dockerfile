#
# corvus - Serve Redis Cluster as Old-school redis
#
FROM iqhive/ubuntu-patched:1804
MAINTAINER Relihan Myburgh <rmyburgh@iqhive.com>

RUN apt-get -y install build-essential automake \
  autoconf libc6-dev-i386 git

RUN mkdir -p /usr/src/;cd /usr/src;git clone https://github.com/eleme/corvus.git

RUN cd /usr/src/corvus; git submodule update --init; make deps; make

RUN mkdir -p /opt/corvus;cp /usr/src/corvus/src/corvus /opt/corvus/corvus

COPY export/ /

ENTRYPOINT /opt/run.sh

